package com.eli173.linksanitizer

import android.widget.TextView
import java.io.BufferedReader
import java.io.InputStreamReader
import java.lang.Error
import java.net.HttpURLConnection
import java.net.URL

// this class handles twitter analytics links
// successfully dodges some tracking due to avoiding loading of the tracking pixel

class AnalyticsHandler(nextHandler: URLHandler, textView: TextView): URLHandler(nextHandler, textView) {
    override val classString: String
        get() = "Analytics handler"

    override fun backgroundTask(url: URL): URL {
        if(url.host != "analytics.twitter.com") {
            return url
        }
        try {
            val conn = url.openConnection() as HttpURLConnection
            conn.connect()
            if((conn.responseCode != 201) and (conn.responseCode != 200)) {
                return url
            }
            val br = BufferedReader(InputStreamReader(conn.inputStream))
            var line = br.readLine()
            while (line != null) {
                if(line.contains("location.replace")) {
                    val firstq = line.indexOfFirst { x -> x=='"' }
                    val lastq = line.indexOfLast { x -> x=='"' }
                    return URL(line.substring(firstq+1, lastq))
                }
                line = br.readLine()
            }
        }
        catch(e: Error) {
            return url
        }
        return url
    }
}